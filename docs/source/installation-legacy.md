# Legacy installation of OpenHI (OpenHI as annotation framework)

This installation is used for legacy version only. See the legacy version at [this link](https://gitlab.com/BioAI/OpenHI/-/tags/OpenHI2).

## The environment
1. Ubuntu 16.08
2. Git
3. Python3 and Pip3
4. MySQL

#### Build the environment (if you have already built, just skip it)
1. Setup an Ubuntu environment
2. Install Git
  * `sudo apt-get update`
  * `sudo apt-get install git`
3. Install Python3 and Pip3
  * ``sudo apt-get install python3 python3-pip``
4. Install MySQL
  * ``` apt-get install mysql-server mysql-client libmysqlclient-dev```
  * ```# You need to set a password for root```

## Clone the project from gitlab
   * ```git clone https://gitlab.com/BioAI/OpenHI.git```
   * ```# The project is a bit large, so clone it may need some time.```

## Install necessary packages
  * ``` apt-get install openslide-tools libsm6```
  * ```pip3 install openslide-python pandas opencv-python numpy flask mysql-connector scikit-image flask_wtf ```

## Build MySQL Database
  * Open mysql command line for example:
    * ```mysql -u root -p```
  * ```source ~/OpenHI/mysql/data_model_4.sql;```

## Change config
  * Find the line: `mydatabase = mysql.connector.connect`
  * Change the config of the mysql after that.
  * For example:
  ```
      host='localhost',
      user='root',
      passwd='666666',
      database='db_wsi2_dev'
  ```

## Run OpenHI (flask framework)
  * ```cd OpenHI```
  * ```python3 app.py```
