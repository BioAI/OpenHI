# OpenHI configuration file

## Location
`static/OpenHI_conf_example.ini` or `static/OpenHI_conf.ini`

The file `static/OpenHI_conf_example.ini` is provided in the source code. However,
the configuration may differ from machine to machine. Therefore we provide local and
external configuration file which is `static/OpenHI_conf.ini`. If local configuration 
is to be used, one should copy the example file and make their own copy of the 
configuration file according to the example file. 

## Database module 
- Name: db
host: MySQL database address
- port: MySQL database port (default = 3306)
- user: Username of the MySQL database
- passed: Password of the MySQL database
- database: MySQL schema name

## Framework
Name: framework
- host: Intended serving address. Use 127.0.0.1 for locally testing on the machine and 0.0.0.0 for local network test.
- port: Testing port. Flask's default is `5000`, HTTP default is `80`. 
- debug: Enable or disable Flask's debug mode with value of 'True' or 'False'.
- reloader: Enable or disable Flask's reloader with value of 'True' or 'False'.

## Annotation project configuration
- Name: project
- pslv: maximum presegmentation level (start from 1)
- pslv_val: sub-region average value of each pslv

Note: pslv = pre-segmentation level

## Annotator viewer initialization configuration
- Name: viewer_init
- slide_id: maximum presegmentation level (start from 1)
- viewer_coor: sub-region average value of each pslv

## SSH configuration used to download metadata from remote server
- Name: viewer_init
- hostname: (Remote server address)
- port: (The port used to download data)
- username: (The username used to connect remote server)
- password: (Password of the user)

## Configuration related to virtual magnification
- Name: virtual_mag
- ignore_human_pixel_size: *(True/False)* Select on or off to ignore human pixel size calculation factor

## Configuration related to performing annotation
- Name: annotation
- manifest_filename: _(string)_ The manifest filename in /framework_src/ directory that you want to use. 
(See manifest example file for formatting)
- max_slide_number: _(int)_ The maximum number of slide allowed to be changed. 
- max_grading: _(int)_ The maximum number of grades that is allowed for annotation. 
- pslv_val: _(Python:list)_ The list of available pre-processed PSLV. 
- grading_specific_check: Enable or disable checking whether the selected region has been labeled only in the same grade , with value of 'True' or 'False'.
- diff_pslv_check: Enable or disable checking whether the selected region has been labeled only in the same PS level, with value of 'True' or 'False'.