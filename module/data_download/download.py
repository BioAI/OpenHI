"""Data downloading module provides a programmatic interface to download whole-slide images from NIH-GDC.
See `NIH-GDC API <https://docs.gdc.cancer.gov/API/Users_Guide/Python_Examples/#downloading-files>`_.
"""
import os
import sys
import requests
import hashlib


class SlideManifest:
    def __init__(self, root_path):
        manifest_path = root_path
        wsis = open(manifest_path).readlines()
        self.manifest = [wsi.split('\t') for wsi in wsis]

    def get_id(self, line):
        return self.manifest[line][0]

    def get_filename(self, line):
        return self.manifest[line][1]

    def get_md5(self, line):
        return self.manifest[line][2]

    def get_size(self, line):
        return self.manifest[line][3]

    def get_state(self, line):
        return self.manifest[line][4]


def download_data(input_file_name):
    """Parse data entry in the input manifest file, create download links, and download WSIs via
    `NIH-GDC API <https://docs.gdc.cancer.gov/API/Users_Guide/Python_Examples/#downloading-files>`_.
    The data will be saved to a directory (`DownloadData/`).

    Example:
        The user should provide the filename of the manifest for this function to work. To run as example, you can use
        :code:`manifest.txt` as an example.

        .. code-block:: python

           download_data('manifest.txt')

    :param str input_file_name: File name of the manifest file (full path if necessary)
    :return: None
    """
    manifest = SlideManifest(input_file_name)    # Initialized the manifest object.
    wsis = open(input_file_name).readlines()
    manifest = SlideManifest('manifest.txt')
    wsis = open('manifest.txt').readlines()

    if not os.path.exists('DownloadData'):
        os.mkdir('DownloadData')

    for i in range(1, len(wsis)):
        url = 'https://api.gdc.cancer.gov/data/' + manifest.get_id(i)
        if not os.path.exists('DownloadData/' + manifest.get_id(i)):
            os.mkdir('DownloadData/' + manifest.get_id(i))
        file_name = 'DownloadData/' + manifest.get_id(i) + '/' + manifest.get_filename(i)
        if os.path.exists(file_name):
            test = hashlib.md5(open(file_name, 'rb').read()).hexdigest()
            if test == manifest.get_md5(i):
                print(str(i) + '/' + str(len(wsis) - 1), url, 'is exists')
                continue
        else:
            test = ""
        while test != manifest.get_md5(i):
            res = requests.get(url, stream=True)
            print(str(i) + '/' + str(len(wsis) - 1), url, res)
            temp_size = 0
            total_size = int(res.headers['content-length'])
            with open(file_name, "wb") as file:
                for chunk in res.iter_content(chunk_size=1024):
                    if chunk:
                        file.write(chunk)
                        temp_size += len(chunk)
                        done = int(50 * temp_size / total_size)
                        sys.stdout.write("\r[%s%s] %d%%   %d/%d" % ('#' * done, ' ' * (50 - done),
                                                                    100 * temp_size / total_size, temp_size,
                                                                    total_size))
                        sys.stdout.flush()
            test = hashlib.md5(open(file_name, 'rb').read()).hexdigest()
        print('\n')


if __name__ == '__main__':
    # download GDC data using the default manifest file (/module/data_download/manifest.txt)
    download_data('manifest.txt')

