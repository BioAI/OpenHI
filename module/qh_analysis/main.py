# Pipeline configuration


# Preprocessing
# - Image normalization
# - Background removal

# Nuclear extraction
# - TODO: 512 x 512 patch extraction
# - TODO: Nuclear segmentation
# result: PNG images

# QH feature extractor (already have 1 nucleus = 1 PNG image)
# result: Array of features (1 array/WSI)

# PCA: Principle Component Analysis - feature selection/analysis (for later)

# Parametric analysis


