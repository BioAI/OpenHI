# coding=gbk
import shutil,os,re,time
import pyvips
import shutil

start =1
stop  =5
now =1
root_folder = '../../framework_src/'
wsi_info = open(root_folder + 'manifest.txt','r')

for each_wsi in wsi_info:
  each_wsi_list = each_wsi.split("\t")
  svs_filename = re.search(r'(TCGA-\w{2}-\w{4})(-)([0-9a-zA-Z-]*)(_)([0-9a-zA-Z-]*)(.svs)', each_wsi_list[1])
  if svs_filename is not None:
    if now>=start and now<=stop:
      if not os.path.exists(root_folder + 'data/' + each_wsi_list[0]):
        shutil.copytree('/home2/djy/TCGA/data/' + each_wsi_list[0],root_folder + 'data/' + each_wsi_list[0])

      print(time.ctime()+" start��"+ str(now) + ":" + each_wsi_list[0])
      img = pyvips.Image.new_from_file(root_folder + 'data/' + each_wsi_list[0]+'/'+ each_wsi_list[1], access='sequential')
      img.dzsave(root_folder + 'dzi_data/' + each_wsi_list[0])
      print(time.ctime()+ " finished: "+ str(now-start+1) + "/" + str(stop-start+1))

    now = now + 1