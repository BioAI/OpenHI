# OpenHI - Open Histopathological Image
![Pipeline status](https://gitlab.com/BioAI/OpenHI/badges/master/pipeline.svg) [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3515375.svg)](https://doi.org/10.5281/zenodo.3515375)

OpenHI is an open source platform for development of digital pathology. OpenHI can: 
1. Annotate with different level of gradings. For example (G1, G2, G3) or (0, 1)
2. Provide different sizes of pre-defined segmentation segments with superpixel segmentation algorithm .
3. Fluidly view the whole-slide image from different vendors. Thanks to [OpenSlide](https://openslide.org)
4. Provide option to create a list of focused region. 
5. Provide accompanying slide information parsed from GDC XML format. 

![OpenHI Sample GUI](docs/source/_static/gui_sample_1.png) 

## Table of Contents
- [Installation](#installation) 
  - [Hardware requirements](#hardware-requirements)
  - [Software requirements](#software-dependencies)
- [Documentation](#documentation)
- Credits: [How to cite OpenHI](#how-to-cite-openhi)
- [License](#license)

## Installation
### Hardware requirements
* Multi-core CPU
* At least 8 GB of memory for OpenHI web framework, and 128GB+ for image pre-processing module. 
* Enough disk space to accommodate the WSI files. 

### Software dependencies
* Clone the repository
* Setup a MySQL server
* Configure the `proc_mysql.py` to accept the new local server
* [Install OpenSlide](https://openslide.org/download/)
* [Install libMI and MMSI](https://gitlab.com/BioAI/libMI#how-to-build)
* [Install pyvips](https://libvips.github.io/libvips/install.html)
  * On macOS, use `brew install vips`
  * On Windows, see official installation guide
* Install necessary python packages using `pip install -r requirements.txt`

### Just install
We have extensively tested OpenHI on Linux environment. However, it is possible to run OpenHI on Windows and Mac as well.
  
1. Install all the dependencies: `pip install -r requirements.txt`
1. Build the MySQL table using script in [_**/sql/**_ folder](module/legacy/mysql). The current version is `data_model_4-1`.
1. Install OpenHI: `python setup.py install` (for development, use `python setup.py develop`)

For detail, see [installation guideline](https://bioai.gitlab.io/OpenHI/installation.html). 

## Documentation 
Documentation is available at [bioai.gitlab.io/OpenHI/](https://bioai.gitlab.io/OpenHI/)

### Project organization
```
.
├── docker -> Docker environment setup
├── docs -> Sphinx documentation
├── framework_src -> OpenHI resource files
├── LICENSE
├── module -> OpenHI modules
├── openhi -> OpenHI library
├── tests -> tests
└── requirements.txt
```

## How to cite OpenHI
Puttapirat Pargorn, Haichuan Zhang, Yuchen Lian, Chunbao Wang, Xiangrong Zhang, Lixia Yao, Chen Li, "OpenHI - An open source framework for annotating histopathological image," 2018 IEEE International Conference on Bioinformatics and Biomedicine (BIBM), Madrid, Spain, 2018, pp. 1076-1082.

doi: 10.1109/BIBM.2018.8621393 <br /> 
[IEEE URL](https://ieeexplore.ieee.org/document/8621393) or [IEEE PDF](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8621393&isnumber=8621069)

## License 

OpenHI is free to use and licensed under GNU LESSER GENERAL PUBLIC LICENSE Version 3. 