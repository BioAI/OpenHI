# OpenHI framework source folder

To run the framework (in demo and testing) some files are needed. This folder is the folder that we keep files to initially run the platform. 

Currently, there are some sub-folders. 

- `annotation_data`: folder to keep annotation data, see its [readme](annotation_data/readme.md) for naming convention for filenames. 
- `bio_data`: biospecimen data downloaded from GDC-TCGA-Biospecimen. [See GDC data dictionary](https://docs.gdc.cancer.gov/Data_Dictionary/viewer/#?view=table-entity-list&anchor=biospecimen). 
- `clinical_data`: clinical data downloaded from GDC-TCGA-Clinical. [See GDC data dictionary](https://docs.gdc.cancer.gov/Data_Dictionary/viewer/#?view=table-entity-list&anchor=clinical). For example, case status, diagnosis, follow-up status. 
- `data`: whole-slide images. For naming convention, see [data readme](data/README.md). 