Read whole-slide images using OpenHI
==============================================
This tutorial will use :code:`slide` module in OpenHI to demonstrate how to read parts of any whole-slide images.

Prerequisites
-------------
#. Some WSI files
#. OpenHI :code:`Slide()` class.

First, we need some WSI files. In your local project, you need to add WSI files according to the manifest file [#mf]_, or edit the manifest file [#mf]_ itself. In this example, we prepare a WSI as follow.

- WSI file: :code:`/framework_src/data/7a29d380-e1b0-4ec3-b6c1-6dc7bb2768f8/TCGA-B0-5088-11A-01-TS1_481546e6-ba46-4c6a-9998-d7d0e0c84b74.svs`
- Manifest file: :code:`/framework_src/manifest.txt`.

And inside the manifest file, we have a record referring to the file.

.. code-block:: text

   id	filename	md5	size	state
   7a29d380-e1b0-4ec3-b6c1-6dc7bb2768f8	TCGA-B0-5088-11A-01-TS1_481546e6-ba46-4c6a-9998-d7d0e0c84b74.svs	74f4fdc6e73b2f5cbb0456dd2f37f7eb	22228545	submitted

This particular WSI file is quite small (22MB). You can download it using `this link <https://api.gdc.cancer.gov/data/7a29d380-e1b0-4ec3-b6c1-6dc7bb2768f8>`_ and try this for yourself.

Now you are set for the next step.

Load slide
----------
Before we load the slide, we need to load :code:`Slide` class from OpenHI library.

.. code-block:: python

   from openhi.img.slide import Slide

Create slide instance with slide ID
###################################
Then, we initialize an instance of the class by assigning a slide ID to the class. In this case, we will assign :code:`id = 1`.

.. code-block:: python

   slide_id = 1
   slide_1 = Slide(slide_id)

The slide should be loaded according to certain configuration: how large is the area we are going to read, and how big the image we want to get. Using :code:`Slide.read_region()` method, we can specify those requirements according to the documentation.

.. automethod:: openhi.img.slide.Slide.read_region
   :noindex:

.. _get-wsi-size:

Get whole-slide image size
##########################
Before we can know which part of the slide we would like to load, we need to know the size of the slide first. To get the size, we use :code:`get_size()` method.

.. code-block:: python

   slide_1_size = slide_1.get_size()
   print('Slide size: ' slide_1_size)
   # Slide size: (18000, 4500)

Configure parameters and load part of slide
###########################################
In this example, the image width and height is 18,000 and 4,500 pixels respectively. We will load the image based on this size. This decision goes into :code:`tl_coor`, :code:`viewing_size`, and :code:`viewer_size`. To understand about differences between *viewing* and *viewer*, see :ref:`ohi-coor`.

For other arguments, we will leave it to default value for now. So, finally, we have.

.. code-block:: python

    # Config the slide loading parameters
    tl_coor = (6000, 3000)
    viewing_size = (400, 400)
    viewer_size = (400, 400)
    flags = 1
    pslv = 0
    grid_density = 10
    internal_check = True

    # Load part of the slide
    part_of_slide = slide_1.read_region(tl_coor, viewing_size, viewer_size, flags, pslv, grid_density, True)
    # imshow_sk(part_of_slide, 1)     # Show the image.

The image shown should look like this.

.. figure:: read-wsi/high-mag.png
    :align: center
    :width: 200px
    :height: 200px

    Part of image. Noted that this image has not been down-sampled since the :code:`viewing_size` and :code:`viewer_size` are equal.

Now, we could try to read large region by reconfiguring the parameters.

.. code-block:: python

    part_of_slide = slide_1.read_region((5000, 1000), (2000, 2000), (600, 600), 1, 0, 10, True)

And we should get this image.
And we should get this image.

.. figure:: read-wsi/low-mag.png
    :align: center
    :width: 200px
    :height: 200px

    This image is, in fact, very large. But it has been resized to the :code:`viewer_size` as you can see that :code:`viewing_size` and :code:`viewer_size` are NOT equal.

OpenHI development image viewing tool
#####################################

To show the image, OpenHI library has a handy tool so that you can show the image just like in MATLAB. You can import that function and use it. It works nicely with IDEs that has built-in support for matplotlib plots.

.. code-block:: python

    from openhi.img.dev_img_toolbox import imshow_sk
    imshow_sk(part_of_slide, 1)

.. image:: read-wsi/dev-tool-view.png

.. note:: The second argument :code:`1` is there to specify that the input image uses BGR color mode.

.. rubric:: Footnotes

.. [#mf] Location of manifest file: :code:`/framework_src/manifest.txt`.