.. This is the main index page

OpenHI Documentation
====================

.. image:: https://gitlab.com/BioAI/OpenHI/badges/master/pipeline.svg
.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3515375.svg
   :target: https://doi.org/10.5281/zenodo.3515375

.. image:: _static/gui_sample_1.png

OpenHI is an open source platform for development of digital pathology. OpenHI can:

1. Annotate with different level of gradings. For example (G1, G2, G3) or (0, 1)
2. Provide different sizes of pre-defined segmentation segments with superpixel segmentation algorithm .
3. Fluidly view the whole-slide image from different vendors. Thanks to `OpenSlide <https://openslide.org>`_
4. Provide option to create a list of focused region.
5. Provide accompanying slide information parsed from GDC XML format.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   modules
   image
   legacy
   tutorials/tutorial-main

How to cite OpenHI
------------------
Puttapirat Pargorn, Haichuan Zhang, Yuchen Lian, Chunbao Wang, Xiangrong Zhang, Lixia Yao, Chen Li, "OpenHI - An open source framework for annotating histopathological image," 2018 IEEE International Conference on Bioinformatics and Biomedicine (BIBM), Madrid, Spain, 2018, pp. 1076-1082.

| DOI: 10.1109/BIBM.2018.8621393
| `IEEE URL <https://ieeexplore.ieee.org/document/8621393>`_ or `IEEE PDF <http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8621393&isnumber=8621069>`_.
| Option: `Proof version (PDF 9.2 MB) <_static/OpenHI-BIBM2018-proof.pdf>`_

Source code
------------------
OpenHI is available at `GitLab <https://gitlab.com/BioAI/OpenHI>`_ under GNU LGPL v3.0.

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
