import os
import configparser

print('This is OpenHI library -- This message is to be removed soon...')

name = 'openhi'
__version__ = '2.0.0'

root_path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/..')

file_checking = root_path + '/module/legacy/static/OpenHI_conf.ini'
checking_status = os.path.isfile(file_checking)
if not checking_status:
    file_checking = root_path + '/module/legacy/static/OpenHI_conf_example.ini'
conf = configparser.ConfigParser()
conf.read(file_checking)
