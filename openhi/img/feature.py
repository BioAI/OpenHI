"""
@Author: JiangBo Shi
@Date: 2020-02-28 20:42:20
@LastEditTime: 2020-03-06 19:08:28
@LastEditors: JiangBo Shi
@Description: This file include some functions to extract some feature described in [1]_.


.. [1]: Tian K, Rubadue C A, Lin D I, et al. Automated clear cell renal carcinoma grade classification with prognostic significance[J]. PloS one, 2019, 14(10).
"""
import skimage
from math import pi, sqrt, log
from skimage.measure import label, regionprops
import matplotlib.pyplot as plt
import numpy as np
from skimage import io
from skimage.draw import ellipse
from skimage.transform import rotate
from skimage.feature import greycomatrix
import numpy as np
from math import log, pi, sqrt
from skimage import measure
from skimage.measure import label, regionprops
from skimage.feature import greycomatrix

import utils
from utils import Hemo_from_HE, V_from_HSV, L_from_Lab
from utils import getGrayLevelRumatrix, calcuteIJ, calcuteS, apply_over_degree


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.2989, 0.5870, 0.1140])


def shape_features(img, inten_img):
    """
    Shape based features.
    :param numpy.ndarray inten_img: Intensity image.
    :param numpy.ndarray img: Labeled input image. Labels with value 0 are ignored.
    :return: a list of shape based features: solidity, eccentricity, equivalent diameter, major/minor axis length,
    areas, perimeter, circularity, compactness, diameter, elongation, max/min/mean intensity,
    :rtype: list
    """
    features = regionprops(img, inten_img)
    circularity = 4 * pi * features[0].area / pow(features[0].perimeter, 2)
    compactness = features[0].area / pow(features[0].perimeter, 2)
    diameter = features[0].perimeter / (2 * pi)
    elongation = features[0].major_axis_length / features[0].perimeter
    dic = {"circularity": circularity, "compactness": compactness, "diameter": diameter,
           "elongation": elongation}
    return [features, dic]


def ccfos_features(img):
    """
    Calculate CCFOS features.
    :param numpy.ndarray img: grayscale image
    :return: dictionary of CCFOS features
    :rtype: dict
    """
    mean = img.sum() / img.size
    temp = pow(img - mean, 2)
    variance = temp.sum() / img.size
    standard_dev = sqrt(variance)
    # smoothness = -1 * pow(standard_dev, 2)
    skewness = pow(img - mean, 3).sum() * pow(standard_dev, -3)
    kurtosis = pow(img - mean, 4).sum() * pow(standard_dev, -4) - 3
    hist = plt.hist(img.ravel(), bins=256)
    energy = pow(hist[0], 2).sum()
    dic = {"mean": mean, "standard_deviation": standard_dev,
           "variance": variance, "skewness": skewness, "kurtosis": kurtosis, "energy": energy}
    return dic


def glcm_features(img, distance=np.array([1]), angles=np.array([0, pi/4, pi/2, 3*pi/4]), levels=8):
    """Calculate GLCM features. The maximum grayscale value in the image should be smaller than the number of levels.

    :param numpy.ndarray img: 2-D image, dtype=numpy.uint8
    :param numpy.ndarray distance: List of pixel pair distance offsets.
    :param numpy.ndarray angles: List of pixel pair angles in radians.
    :param int levels: The input image should contain integers in [0, levels-1], where levels indicate the number of
    grey-levels counted (typically 256 for an 8-bit image). This argument is required for 16-bit images or higher and is
    typically the maximum of the image. As the output matrix is at least levels x levels, it might be preferable to use
    binning of the input image rather than large values for levels.
    :return: Dictionary of GLCM features.
    :rtype: dict
    """
    img_convert = np.array(img * levels / 256, dtype=np.uint8)  # [0, 256] --> [0, levels]
    GLCM = greycomatrix(img_convert, distance, angles, levels=levels)  # Calculate glcm matrix

    # Initialize mean & variance & standard variance
    mean_i = np.zeros((len(distance), len(angles)))
    mean_j = np.zeros((len(distance), len(angles)))
    variance_i = np.zeros((len(distance), len(angles)))
    variance_j = np.zeros((len(distance), len(angles)))
    stand_variance_i = np.zeros((len(distance), len(angles)))
    stand_variance_j = np.zeros((len(distance), len(angles)))

    # Initialize Contrast, Correlation, Dissimilarity, Homogeneity, ASM, Energy, Entropy
    contrast = np.zeros((len(distance), len(angles)))
    correlation = np.zeros((len(distance), len(angles)))
    dissimilarity = np.zeros((len(distance), len(angles)))
    homogeneity = np.zeros((len(distance), len(angles)))
    ASM = np.zeros((len(distance), len(angles)))
    energy = np.zeros((len(distance), len(angles)))
    # entropy = np.zeros((len(distance), len(angles)))

    homogeneityII = np.zeros((len(distance), len(angles)))
    autocorrelation = np.zeros((len(distance), len(angles)))
    correlationII = np.zeros((len(distance), len(angles)))
    cluster_prominence = np.zeros((len(distance), len(angles)))
    cluster_shade = np.zeros((len(distance), len(angles)))
    maximum_prob = np.zeros((len(distance), len(angles)))

    # For each distance & each angles, calculate corresponding GLCM features
    for dist in range(len(distance)):
        for ang in range(len(angles)):
            # Express the GLCM as a probability
            glcm = GLCM[:, :, dist, ang]
            glcm = glcm / np.sum(glcm)

            # Calculate mean_i, mean_j
            for i in range(levels):
                for j in range(levels):
                    mean_i[dist, ang] += i * glcm[i, j]
                    mean_j[dist, ang] += j * glcm[i, j]

            # Calculate variance_i, variance_j
            for i in range(levels):
                for j in range(levels):
                    variance_i[dist, ang] += glcm[i, j] * pow(i - mean_i[dist, ang], 2)
                    variance_j[dist, ang] += glcm[i, j] * pow(j - mean_j[dist, ang], 2)

            # Calculate standard variance
            stand_variance_i[dist, ang] = sqrt(variance_i[dist, ang])
            stand_variance_j[dist, ang] = sqrt(variance_j[dist, ang])

            # Calculate
            for i in range(levels):
                for j in range(levels):
                    contrast[dist, ang] += glcm[i, j] * pow(i - j, 2)
                    correlation[dist, ang] += glcm[i, j] * (i - mean_i[dist, ang]) * (j - mean_j[dist, ang]) \
                                              / (stand_variance_i[dist, ang] * stand_variance_j[dist, ang])
                    dissimilarity[dist, ang] += glcm[i, j] * abs(i - j)
                    homogeneity[dist, ang] += glcm[i, j] / (1 + pow(i - j, 2))
                    ASM[dist, ang] += pow(glcm[i, j], 2)
                    # entropy[dist, ang] += -1 * glcm[i, j] * log(glcm[i, j])  # glcm[i, j] may equal to 0,
                    # thus raising math domain error

                    homogeneityII[dist, ang] += glcm[i, j] / (1 + abs(i - j))
                    autocorrelation[dist, ang] += i * j * glcm[i, j]
                    correlationII[dist, ang] += (i * j * glcm[i, j] - mean_i[dist, ang] * mean_j[dist, ang]) \
                                                / (stand_variance_i[dist, ang] * stand_variance_j[dist, ang])
                    cluster_prominence += pow(i + j - mean_i[dist, ang] - mean_j[dist, ang], 4) * glcm[i, j]
                    cluster_shade += pow(i + j - mean_i[dist, ang] - mean_j[dist, ang], 3) * glcm[i, j]

            energy[dist, ang] = sqrt(ASM[dist, ang])
            maximum_prob[dist, ang] = np.max(glcm)

    dic = {"GLCM": GLCM, "mean_i": mean_i, "mean_j": mean_j, "variance_i": variance_i, "variance_j": variance_j,
           "standard_variance_i": stand_variance_i, "standard_variance_j": stand_variance_j, "contrast": contrast,
           "correlation": correlation, "correlationII": correlationII, "dissimilarity": dissimilarity,
           "homogeneity": homogeneity, "homogeneityII": homogeneityII, "ASM": ASM, "energy": energy,
           "autocorrelation": autocorrelation, "cluster_prominence": cluster_prominence, "cluster_shade": cluster_shade,
           "maximum_prob": maximum_prob}

    return dic


# GLRLM (Hong Bangyang)
class glrlm:

    def extract_glrlm(self, array, theta):
        '''
        calculate glrlm for images
        parameter：
        array: Input，the image
        theta: Input，The Angle used in calculating glrlm，the type is list，Inclinable field：['deg0', 'deg45', 'deg90', 'deg135']
        glrlm: Output
        '''
        P = array
        x, y = P.shape
        min_pixels = np.min(P)  # The smallest pixel in the image
        run_length = max(x, y)  # Maximum parade length of pixels
        num_level = np.max(P) - np.min(P) + 1  # The gray scale of the image

        deg0 = [val.tolist() for sublist in np.vsplit(P, x) for val in sublist]  # 0 degree matrix statistics
        # print(deg0)
        deg90 = [val.tolist() for sublist in np.split(np.transpose(P), y) for val in
                 sublist]  # 90 degree matrix statistics
        # print(deg90)
        diags = [P[::-1, :].diagonal(i) for i in range(-P.shape[0] + 1, P.shape[1])]  # 45 degree matrix statistics
        deg45 = [n.tolist() for n in diags]
        # print(deg45)
        Pt = np.rot90(P, 3)  # 135 degree matrix statistics
        diags = [Pt[::-1, :].diagonal(i) for i in range(-Pt.shape[0] + 1, Pt.shape[1])]
        deg135 = [n.tolist() for n in diags]

        # print(deg135)

        def length(l):
            if hasattr(l, '__len__'):
                return np.size(l)
            else:
                i = 0
                for _ in l:
                    i += 1
                return i

        glrlm = np.zeros((num_level, run_length, len(theta)))
        for angle in theta:
            for splitvec in range(0, len(eval(angle))):
                flattened = eval(angle)[splitvec]
                answer = []
                for key, iter in groupby(flattened):
                    answer.append((key, length(iter)))
                for ansIndex in range(0, len(answer)):
                    glrlm[int(answer[ansIndex][0] - min_pixels), int(answer[ansIndex][1] - 1), theta.index(
                        angle)] += 1
        return glrlm

    def apply_over_degree(self, function, x1, x2):
        rows, cols, nums = x1.shape
        result = np.ndarray((rows, cols, nums))
        for i in range(nums):
            # print(x1[:, :, i])
            result[:, :, i] = function(x1[:, :, i], x2)
            # print(result[:, :, i])
        result[result == np.inf] = 0
        result[np.isnan(result)] = 0
        return result

    def calcuteIJ(self, rlmatrix):
        gray_level, run_length, _ = rlmatrix.shape
        I, J = np.ogrid[0:gray_level, 0:run_length]
        return I, J + 1

    def calcuteS(self, rlmatrix):
        return np.apply_over_axes(np.sum, rlmatrix, axes=(0, 1))[0, 0]

    # SRE
    def getShortRunEmphasis(self, rlmatrix):
        I, J = self.calcuteIJ(rlmatrix)
        numerator = np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, rlmatrix, (J * J)), axes=(0, 1))[0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # LRE
    def getLongRunEmphasis(self, rlmatrix):
        I, J = self.calcuteIJ(rlmatrix)
        numerator = np.apply_over_axes(np.sum, self.apply_over_degree(np.multiply, rlmatrix, (J * J)), axes=(0, 1))[
            0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # GLN
    def getGrayLevelNonUniformity(self, rlmatrix):
        G = np.apply_over_axes(np.sum, rlmatrix, axes=1)
        numerator = np.apply_over_axes(np.sum, (G * G), axes=(0, 1))[0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # RLN
    def getRunLengthNonUniformity(self, rlmatrix):
        R = np.apply_over_axes(np.sum, rlmatrix, axes=0)
        numerator = np.apply_over_axes(np.sum, (R * R), axes=(0, 1))[0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # RP
    def getRunPercentage(self, rlmatrix):
        gray_level, run_length, _ = rlmatrix.shape
        num_voxels = gray_level * run_length
        return self.calcuteS(rlmatrix) / num_voxels

    # LGLRE
    def getLowGrayLevelRunEmphasis(self, rlmatrix):
        I, J = self.calcuteIJ(rlmatrix)
        numerator = np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, rlmatrix, (I * I)), axes=(0, 1))[
            0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # HGLRE
    def getHighGrayLevelRunEmphais(self, rlmatrix):
        I, J = self.calcuteIJ(rlmatrix)
        numerator = np.apply_over_axes(np.sum, self.apply_over_degree(np.multiply, rlmatrix, (I * I)), axes=(0, 1))[
            0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # SRLGLE
    def getShortRunLowGrayLevelEmphasis(self, rlmatrix):
        I, J = self.calcuteIJ(rlmatrix)
        numerator = \
            np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, rlmatrix, (I * I * J * J)), axes=(0, 1))[0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # SRHGLE
    def getShortRunHighGrayLevelEmphasis(self, rlmatrix):
        I, J = self.calcuteIJ(rlmatrix)
        temp = self.apply_over_degree(np.multiply, rlmatrix, (I * I))
        numerator = np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, temp, (J * J)), axes=(0, 1))[0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # LRLGLE
    def getLongRunLow(self, rlmatrix):
        I, J = self.calcuteIJ(rlmatrix)
        temp = self.apply_over_degree(np.multiply, rlmatrix, (J * J), axes=(0, 1))
        numerator = np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, temp, (J * J)), axes=(0, 1))[0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S

    # LRHGLE
    def getLongRunHighGrayLevelEmphais(self, rlmatrix):
        I, J = self.calcuteIJ(rlmatrix)
        numerator = \
            np.apply_over_axes(np.sum, self.apply_over_degree(np.multiply, rlmatrix, (I * I * J * J)), axes=(0, 1))[
                0, 0]
        S = self.calcuteS(rlmatrix)
        return numerator / S


def shape_feature(boundary_path, i):
    '''
    @description: nucleus shape features
    @param {numpy.ndarray} boundary_path: Path of boundary file.
    @param {int} i: the number of nucleus
    @return: a dictionary of shape based features
    @rtype: dictionary
    '''
    boundary = utils.read_txt(boundary_path)
    image = utils.image_boundary_normalization(boundary)

    labels = measure.label(image)
    features = regionprops(labels)
    area = features[i].area
    perimeter = features[i].perimeter
    equivalent_spherical_perimeter = 4 * np.pi * np.power(np.power(features[0].area * 3 / 4 / np.pi, 1 / 3),
                                                          2)  # equivalent spherical perimeter

    minr, minc, maxr, maxc = features[i].bbox
    width = maxc - minc
    height = maxr - minr
    bounding_rectangle = (width, height)

    fit_ellipse = (features[i].major_axis_length, features[i].minor_axis_length)
    circularity = 4 * np.pi * features[0].area / pow(features[i].perimeter, 2)
    aspect_ratio = width / height
    solodity = features[i].solidity
    roundness = 4 * np.pi * area / (perimeter * perimeter)
    Ferets_diameter = perimeter / np.pi
    dic = {
        "area": area,
        "perimeter": perimeter,
        "equivalent_spherical_perimeter": equivalent_spherical_perimeter,
        "bounding_rectangle": bounding_rectangle,
        "fit_ellipse": fit_ellipse,
        "circularity": circularity,
        "aspect_ratio": aspect_ratio,
        "roundness": roundness,
        "solodity": solodity,
        "Ferets_diameter": Ferets_diameter
    }
    return dic


def intensity_feature(image_path, boundary_path, i, color_space):
    '''
    @description: nucles intensity feature.
    @param {type} image_path: histopathological image path
    @param {type} boundary_path: path of boundary file
    @param {type} i: numer of which nucleus
    @param {type} color_space: select which color space. 0 represent H channel in H&E color space;
                               1 represent V channel in HSV color space; 2 represent L channel in Lab color space.
    @return: a dictionary of intensity based features
    @rtype: dictionary
    '''
    if (color_space == 0):
        select_color_channel = Hemo_from_HE(image_path)
    elif (color_space == 1):
        select_color_channel = V_from_HSV(image_path)
    elif (color_space == 2):
        select_color_channel = L_from_Lab(image_path)

    boundary = utils.read_txt(boundary_path)
    boundary_n = utils.image_boundary_normalization(boundary)

    labels = measure.label(boundary_n)
    features = regionprops(labels, select_color_channel)
    nucleus_bb = features[i].intensity_image
    N = features[i].area
    mean = np.sum(nucleus_bb) / N
    nonzero_cord = np.nonzero(nucleus_bb)
    num = nonzero_cord[0].shape[0]
    temp = np.zeros((num))
    kurtosis_part = 0.
    for i in range(num):
        temp[i] = nucleus_bb[nonzero_cord[0][i], nonzero_cord[1][i]]
        kurtosis_part = kurtosis_part + np.power((temp[i] - mean), 4)
    median = np.median(temp)
    standard_deviation = np.std(temp)
    skewness = 3 * (mean - median) / standard_deviation
    kurtosis = kurtosis_part / num / np.power(standard_deviation, 4)
    dic = {
        "mean": mean,
        "median": median,
        "standard_deviation": standard_deviation,
        "skewness": skewness,
        "kurtosis": kurtosis
    }
    return dic


def Co_occurrence_based_features(img, distance=np.array([1]), angles=np.array([0, pi / 4, pi / 2, 3 * pi / 4]),
                                 levels=8):
    img_convert = np.array(img * levels / 256, dtype=np.uint8)  # [0, 256] --> [0, levels]
    GLCM = greycomatrix(img_convert, distance, angles, levels=levels)  # Calculate glcm matrix

    # Initialize mean & variance & standard variance
    mean_i = np.zeros((len(distance), len(angles)))
    mean_j = np.zeros((len(distance), len(angles)))
    variance_i = np.zeros((len(distance), len(angles)))
    variance_j = np.zeros((len(distance), len(angles)))
    stand_variance_i = np.zeros((len(distance), len(angles)))
    stand_variance_j = np.zeros((len(distance), len(angles)))

    # Initilize correlation, energy, entropy
    correlation = np.zeros((len(distance), len(angles)))
    cluster_shade = np.zeros((len(distance), len(angles)))
    cluster_prominence = np.zeros((len(distance), len(angles)))
    energy = np.zeros((len(distance), len(angles)))
    entropy = np.zeros((len(distance), len(angles)))
    haralick_correlation = np.zeros((len(distance), len(angles)))
    inertia = np.zeros((len(distance), len(angles)))
    incerse_difference_moment = np.zeros((len(distance), len(angles)))

    # For each distance & each angles, calculate corresponding GLCM features
    for dist in range(len(distance)):
        for ang in range(len(angles)):
            # Express the GLCM as a probability
            glcm = GLCM[:, :, dist, ang]
            glcm = glcm / np.sum(glcm)

            # Calculate mean_i, mean_j
            for i in range(levels):
                for j in range(levels):
                    mean_i[dist, ang] += i * glcm[i, j]
                    mean_j[dist, ang] += j * glcm[i, j]

            # Calculate variance_i, variance_j
            for i in range(levels):
                for j in range(levels):
                    variance_i[dist, ang] += glcm[i, j] * pow(
                        i - mean_i[dist, ang], 2)
                    variance_j[dist, ang] += glcm[i, j] * pow(
                        j - mean_j[dist, ang], 2)

                    # Calculate standard variance
            stand_variance_i[dist, ang] = sqrt(variance_i[dist, ang])
            stand_variance_j[dist, ang] = sqrt(variance_j[dist, ang])

        for i in range(levels):
            for j in range(levels):
                correlation[dist, ang] += glcm[i, j] * (i - mean_i[dist, ang]) * (j - mean_j[dist, ang]) \
                                          / (stand_variance_i[dist, ang] * stand_variance_j[dist, ang])
                cluster_prominence += pow(
                    i + j - mean_i[dist, ang] - mean_j[dist, ang],
                    4) * glcm[i, j]
                cluster_shade += pow(
                    i + j - mean_i[dist, ang] - mean_j[dist, ang],
                    3) * glcm[i, j]
                energy[dist, ang] += pow(glcm[i, j], 2)
                haralick_correlation[dist, ang] += (i * j * glcm[i, j] - mean_i[dist, ang] * mean_j[dist, ang]) \
                                                   / (stand_variance_i[dist, ang] * stand_variance_j[dist, ang])
                inertia[dist, ang] += pow(i - j, 2) * glcm[i, j]
                incerse_difference_moment[dist, ang] = glcm[i, j] / (1 + pow((i - j), 2))
                if (glcm[dist, ang] == 0):
                    entropy[dist, ang] += 0
                else:
                    entropy[dist, ang] += -glcm[i, j] * log(glcm[i, j], 2)

        dic = {
            "correlation": correlation,
            "cluster_shade": cluster_shade,
            "cluster_prominence": cluster_prominence,
            "energy": energy,
            "entropy": entropy,
            "haralick_correlation": haralick_correlation,
            "inertia": inertia,
            "incerse_difference_moment": incerse_difference_moment
        }
        return dic


def run_length_based_feature(array, theta):
    rlmatrix = getGrayLevelRumatrix(array, theta)
    s = calcuteS(rlmatrix)
    gray_level_non_uniformity = np.apply_over_axes(np.sum, (np.apply_over_axes(np.sum, rlmatrix, axes=1)
                                                            * np.apply_over_axes(np.sum, rlmatrix, axes=1)),
                                                   axes=(0, 1))[0, 0] / s
    run_length_non_uniformity = np.apply_over_axes(np.sum, (np.apply_over_axes(np.sum, rlmatrix, axes=0)
                                                            * np.apply_over_axes(np.sum, rlmatrix, axes=0)),
                                                   axes=(0, 1))[0, 0] / s
    I, J = calcuteIJ(rlmatrix)
    low_gray_level_run_emphasis = \
    np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, rlmatrix, (I * I)), axes=(0, 1))[0, 0] / s
    high_gray_level_run_emphasis = \
    np.apply_over_axes(np.sum, self.apply_over_degree(np.multiply, rlmatrix, (I * I)), axes=(0, 1))[0, 0] / s
    short_run_low_gray_level_emphasis = numerator = \
    np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, rlmatrix, (I * I * J * J)),
                       axes=(0, 1))[0, 0] / s
    short_run_high_gray_level_emphasis = \
    np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, self.apply_over_degree(np.multiply,
                                                                                        rlmatrix, (I * I)), (J * J)),
                       axes=(0, 1))[0, 0] / s
    long_run_low_gray_level_emphasis = \
    np.apply_over_axes(np.sum, self.apply_over_degree(np.divide, self.apply_over_degree(np.multiply,
                                                                                        rlmatrix, (J * J), axes=(0, 1)),
                                                      (J * J)), axes=(0, 1))[0, 0] / s
    long_run_high_gray_level_emphasis = \
    np.apply_over_axes(np.sum, self.apply_over_degree(np.multiply, rlmatrix, (I * I * J * J)), axes=(0, 1))[0, 0] / s
    dic = {
        "gray_level_non_uniformity": gray_level_non_uniformity,
        "run_length_non_uniformity": run_length_non_uniformity,
        "low_gray_level_run_emphasis": low_gray_level_run_emphasis,
        "high_gray_level_run_emphasis": high_gray_level_run_emphasis,
        "short_run_low_gray_level_emphasis": short_run_low_gray_level_emphasis,
        "short_run_high_gray_level_emphasis": short_run_high_gray_level_emphasis,
        "long_run_low_gray_level_emphasis": long_run_low_gray_level_emphasis,
        "long_run_high_gray_level_emphasis": long_run_high_gray_level_emphasis
    }
    return dic


if __name__ == '__main__':
    # Test Code
    image = np.zeros((256, 256))

    rr, cc = ellipse(140, 130, 50, 70)
    image[rr, cc] = 1

    image = rotate(image, angle=15, order=0)

    label_img = label(image)
    regions = shape_features(label_img, image)

    test_image = io.imread('test.png')
    test_image_gray = rgb2gray(test_image)
    test_image_gray_int8 = np.array(test_image_gray, dtype=np.uint8)
    test_image_conv = test_image_gray_int8 / 32
    test_image_conv = np.array(test_image_conv, dtype=np.uint8)

    GLCM_features = glcm_features(test_image_gray)

    # GLRLM example (Hong Bangyang)
    theta = ['deg0', 'deg45', 'deg90', 'deg135']
    a = [[0, 0, 1, 1, 2, 2, 3, 3],
         [1, 1, 2, 2, 3, 3, 0, 0],
         [2, 2, 3, 3, 0, 0, 1, 1],
         [3, 3, 0, 0, 1, 1, 2, 2]]
    c = [[1, 1, 0, 0],
         [1, 1, 0, 0],
         [0, 0, 2, 2],
         [0, 0, 2, 2]]
    b = np.array(c)
    x = glrlm()
    g = x.extract_glrlm(b, theta)
    sre = x.getShortRunEmphasis(g)
    lre = x.getLongRunEmphasis(g)
    lrhgle = x.getLongRunHighGrayLevelEmphais(g)
    print(sre)
    print(lre)
    print(lrhgle)

