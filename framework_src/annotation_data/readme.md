# Annotation data structure
- Slide-UUID
    - r{region_id}.txt
    - r{region_id}.png
    - a{annotator_id}_r{region_id}.txt
    - a{annotator_id}_whole_slide.txt

    
# File name explaination
- Slide-UUID is the uuid of the slide in the manifest file.
    - r{region_id}.txt
        - The pixel and its nuclei id will be saved in this file. 
        - It is named like r1.txt 
    - r{region_id}.png
        - The original picture will be saved in this file. 
        - It is named like r1.png 
    - a{annotator_id}_r{region_id}.txt
        - The nuclei id and its grading will be saved in this file. 
        - It is named like a1_r1.txt 
    - a{annotator_id}_whole_slide.txt
        - The whole_slide information will be saved in this file. 
        - It is named like a1_whole_slide.txt
        
# Example
- eb25474f-e249-4f39-9339-b8f5c1690610
    - r1.txt
    - r1.png
    - a1_r1.txt
    - a2_r1.txt
    - a3_r1.txt
    - r2.txt
    - r2.png
    - a1_r2.txt
    - a2_r2.txt
    - a3_r2.txt
    - a1_whole_slide.txt
    - a2_whole_slide.txt