"""
This is the legacy module to read annotation data from OpenHIv1 datamodel.

Operations
----------
- Initialized the framework according to the configuration
- Configure the exporting annotation (annotator, slide, diagnostic region)
- Read the original image and annotation
- Save the result
"""

import os
import configparser
import openhi.legacy.anno_img as img
import openhi.legacy.anno_web as web
import openhi.legacy.anno_sql as sql
from openhi.legacy.anno_web import Clr


# Internal functions
def _configuration_load():
    # Read framework configuration from INI configuration file.
    # --Check if the local configuration file exists, if not run based on the example file.
    file_checking = '/home2/sjb/OpenHI/module/legacy/static/OpenHI_conf.ini'
    checking_status_conf_file = os.path.isfile(file_checking)
    if not checking_status_conf_file:
        open_file_name = '/home2/sjb/OpenHI/module/legacy/static/OpenHI_conf_example.ini'
    else:
        open_file_name = file_checking

    # --Read the OpenHI INI configuration file.
    _conf = configparser.ConfigParser()
    _conf.read(open_file_name)

    return _conf


# Initialization of the framework
conf = _configuration_load()        # Load the configuration file

# Check for cache directory
dir_checking = 'static/cache'
checking_status = os.path.exists(dir_checking)

print('Did the checking directory exists?: ' + str(checking_status))    # Print the checking result
if not checking_status:
    os.mkdir(dir_checking)
    print(Clr.BOLD + 'The directory "' + dir_checking + '" did not exists and it has been created.' + Clr.end)
else:
    print(Clr.BOLD + 'The directory "' + dir_checking + '" already exists. No further actions are required.' + Clr.end)

rand_url = web.WebInter()               # rand_url stands for random URL
db = sql.create_connector()             # db stands for database
asess = web.AnnotatorSessionList()      # assess stands for annotator session

# Initialize annotators
init_vp = img.ViewingPosition()     # vp is 'viewing position'
manifest_line_number = int(conf['viewer_init']['slide_id']) - 1
init_vp.load_from_config(conf['viewer_init']['viewer_coor'])
asess.init_new_annotator(1, int(conf['viewer_init']['slide_id']), init_vp, sql.re_create_connector(db))
asess.init_new_annotator(2, int(conf['viewer_init']['slide_id']), init_vp, sql.re_create_connector(db))

# Load the first WSIs (no overlaying layer)
img.call_wsi(asess.get_loader_obj(1), init_vp.coor_tl, init_vp.size_viewing, init_vp.size_viewer)
img.wsi_get_thumbnail(asess.get_loader_obj(1), rand_url.current, init_vp.size_viewer)
print('first image is saved as: ' + rand_url.current)

# ---- [End] Initializing the framework ----- #
