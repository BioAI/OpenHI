


# MySQL Database Information
In this documentation the data model of the framework is specified. 


## Overall data table sample

| Key | Description | Attribute | Remarks | 
| --- | --- | --- | --- |
| pt_id | ID of each annotation point (x,y) | PRIMARY KEY | N/A |
| x | x-coordinate | INT | This INT could be very large due to image dimension | 
| y | y-coordinate | INT | "--" |
| grading | grading of the area | INT | N/A | 
| pre-segmentation density | The pre-segmentation density that has been chosen at the time of annotation | INT | n/a |
| timestamp | Annotation timestamp | DATETIME | Just the timestamp | 
| image_number | Project's internal image id | INT | 1-1650 |
| TCGA_UUID | TCGA UUID | CHAR | not sure if it is the same length | 
| annotator | Unique signature of the pathologist who annotate the specific point | N/A | N/A |

### Detailed data modelling

#### Table 1: annotation point
| Column name   | Data type | Options | Default expression | Description | 
| -----------   | ---       | --- | --- | --- |
| pt_id         | INT       | PRIMARY KEY, UNSIGNED, AUTO_INCREMENT | NOT NULL | N/A |
| x             | INT       | INDEX, UNSIGNED | NOT NULL | N/A |
| y             | INT       | INDEX, UNSIGNED | NOT NULL | N/A |
| annotation_ts | DATETIME  |  N/A  | NOT NULL | N/A |
| grading_id    | TINYINT   | FOREIGN KEY, UNSIGNED | NOT NULL | N/A | 
| pslv_id       | TINYINT   | FOREIGN KEY, UNSIGNED | NOT NULL |  N/A |
| slide_id      | SMALLINT  | FOREIGN KEY, UNSIGNED | NOT NULL | N/A |
| annotator_id  | SMALLINT  | FOREIGN KEY, UNSIGNED | NOT NULL | N/A |
| region_id     | INT       | UNSIGNED | NOT NULL | Unique id of the c-cluster |
| selected      | TINYINT   | UNSIGNED | NOT NULL | The status of the point showing if it is representing the c-cluster |
| anno_batch    | INT | UNSIGNED | NOT NULL | Unique id of each specific annotating action |


#### Table 2: wsi

| Column name | Data type | Options | Default expression |
| --- | --- | --- | ---|
| slide_id | SMALLINT | PRIMARY KEY, UNSIGNED, AUTO_INCREMENT | NOT NULL |
| tcga_wsi_id | VARCHAR(255) | UNIQUE | 'N/A' |
| tcga_case_id | VARCHAR(255) | UNIQUE | 'N/A' |

#### Table 3: annotator

| Column name | Data type | Options | Default expression |
| --- | --- | --- | ---|
| annotator_id | SMALLINT | PRIMARY KEY, UNSIGNED, AUTO_INCREMENT=1 | NOT NULL |
| codename | VARCHAR(45) | N/A | 'N/A' | 

#### Table 4: grading

| Column name | Data type | Options | Default expression |
| --- | --- | --- | ---|
| grading_id | TINYINT | PRIMARY KEY, UNSIGNED, AUTO_INCREMENT | NOT NULL |
| grading_std_name | VARCHAR(255) | N/A | 'N/A' | 

#### Table 5: pslv (pre_segmentation_level)

| Column name | Data type | Options | Default expression |
| --- | --- | --- | --- |
| pslv_id | TINYINT | PRIMARY KEY, UNSIGNED, AUTO_INCREMENT | NOT NULL |
| subregion_density | INT | UNSIGNED | NOT NULL |

