import cv2
import numpy as np


def _compute_space_score_fscore(img1, img2):
    img1gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    ret1, mask1 = cv2.threshold(img1gray, 10, 255, cv2.THRESH_BINARY)
    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret2, mask2 = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    and_mask1_mask2 = cv2.bitwise_and(mask1, mask2)
    and_nzCount = cv2.countNonZero(and_mask1_mask2)  # value of 11
    num11 = and_nzCount
    image_one_zero = cv2.subtract(mask1, and_mask1_mask2)
    num10 = cv2.countNonZero(image_one_zero)
    image_zero_one = cv2.subtract(mask2, and_mask1_mask2)
    num01 = cv2.countNonZero(image_zero_one)
    image_zero_zero = cv2.bitwise_not(cv2.bitwise_or(mask1, mask2))
    num00 = cv2.countNonZero(image_zero_zero)
    TP = num11
    FP = num01
    FN = num10
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    f_score = 2*precision*recall/(precision+recall)
    return f_score


def compute_space_grade_fscore(img1, img2, grade_num):
    """
    This is to compute the space score between two images using fscore as the metrics. Only consider specific grade level.
    :param img1: first image
    :param img2: second image
    :param grade_num: which grade
    :return: f score
    """
    if grade_num == 1:
        B, G, R = 124, 252, 0  # represents the color of grade 1(green)
    elif grade_num == 2:
        B, G, R = 0, 255, 255  # represents the color of grade 2(yellow)
    img1[np.where((img1 != [B, G, R]).all(axis=2))] = [0, 0, 0]
    img2[np.where((img2 != [B, G, R]).all(axis=2))] = [0, 0, 0]
    fscore = _compute_space_score_fscore(img1, img2)
    return fscore


if __name__ == '__main__':
    img1 = cv2.imread('data/anno_a1s1p6.png')
    img2 = cv2.imread('data/anno_a2s1p6.png')
    fscore = compute_space_grade_fscore(img1, img2, 1)
    print(fscore)

