from Slide import Slide
from MysqlConnector import MysqlConnector
from SqliteConnector import SqliteConnector
import mmsi
import os
import numpy as np
import imageio


class AnnotationResult:
    """A class exporting annotation data.


    """

    def __init__(self, slide_index: int, slide_root_path: str):
        self.slide_index = slide_index
        self.wsi = Slide(self.slide_index, slide_root_path)
        self.db = MysqlConnector()
        self.mmsi_obj = None
        self.DAA_root_path = 'anno_data/'
        self.WSA_root_path = './'
        self.WSA_path = './'  + self.wsi.slide_id +'/'
        self.mmsi_path = self.WSA_path + 'nuclei_id.mmsi'

    def __del__(self):
        if self.mmsi_obj is not None:
            self.mmsi_obj.Close()
        return None

    def create_mmsi(self, WSA_root_path: str = None) -> str:
        """
        create the mmsi file in to the file_path.

        :param WSA_root_path: the place that the mmsi is saved to. end with '/'
        :return: file name.
        """
        if WSA_root_path is not None:
            self.WSA_root_path = WSA_root_path
        if not os.path.exists(self.WSA_root_path):
            os.mkdir(self.WSA_root_path)

        self.WSA_path = self.WSA_root_path + self.wsi.slide_id + '/'

        self.mmsi_path = self.WSA_path + 'nucleus_id.mmsi'
        mmsi_writer= mmsi.Writer(self.mmsi_path)
        mmsi_writer.SetImageSize(self.wsi.get_size())
        mmsi_writer.SetTileSize((256, 256))
        mmsi_writer.SetPixelSize(8)
        mmsi_writer.Open()
        mmsi_writer.SetPropertyValue('maxNucleusID',str(0))
        mmsi_writer.Close()
        return self.mmsi_path

    def open_mmsi(self, mmsi_path: str = None):
        if mmsi_path is not None:
            self.mmsi_path = mmsi_path
        self.mmsi_obj = mmsi.Accessor(self.mmsi_path)
        self.mmsi_obj.Open()
        self.mmsi_obj.SetSecureMode(True)
        self.mmsi_obj.SetCompressLevel(9)
        return self.mmsi_obj

    def regionID_From_DAA_To_WSA(self, region_id: int):
        centre_x,centre_y = self.db.get_center_tb_reg(self.slide_index, region_id)

        region_image_url = self.DAA_root_path + 's' + str(self.slide_index) + '_r' + str(region_id) + '.txt'
        # print(region_image_url)
        if not os.path.exists(region_image_url):
            return
        region_image = np.loadtxt(region_image_url, delimiter=",", dtype=int)

        maxNucleusID = int(self.mmsi_obj.GetPropertyValue('maxNucleusID'))

        region_image[region_image <= 1] = 0
        region_image[region_image != 0] += (maxNucleusID-1)
        maxNucleusID = region_image.max()
        self.mmsi_obj.SetPropertyValue('maxNucleusID',str(maxNucleusID))
        # print(maxNucleusID)
        self.mmsi_obj.SetCurrentRegion((centre_x - 256, centre_y - 256), (512, 512));
        self.mmsi_obj.WriteRegion(0,region_image,None)
        # print(self.mmsi_obj.ReadRegion(0))

    def nucleusGrade_From_DAA_To_WSA(self,annotator_id :int, region_id: int):
        annotation_data_url = self.DAA_root_path + 'a' + str(annotator_id) + '_s' + str(self.slide_index) + '_r' + str(region_id) + '.txt'
        print(annotation_data_url)
        if not os.path.exists(annotation_data_url):
            return
        annotation_data = np.loadtxt(annotation_data_url, delimiter=",", dtype=int)
        maxNucleusID = int(self.mmsi_obj.GetPropertyValue('maxNucleusID'))

        db = SqliteConnector(self.WSA_path + 'annotator_' + str(annotator_id) + '.db')

        for idx, val in enumerate(annotation_data, -1):
            if idx <= 0:
                continue
            print(idx + maxNucleusID , val)
            db.incert_NucleusGrade(idx + maxNucleusID, int(val))

        centre_x,centre_y = self.db.get_center_tb_reg(self.slide_index, region_id)
        db.incert_RegionCentre(region_id,centre_x,centre_y)
        # print(db.get_all_NucleusGrade())
        # print(db.get_NucleusGrade(3))

    def export_DAA_To_WSA(self,annotatorID : int):
        self.create_mmsi('./')
        self.open_mmsi()
        for i in self.db.get_tba_list(self.slide_index):
            regionID = i[0]
            self.nucleusGrade_From_DAA_To_WSA(annotatorID, regionID)
            self.regionID_From_DAA_To_WSA(regionID)



