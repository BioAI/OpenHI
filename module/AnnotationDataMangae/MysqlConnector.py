import os
import configparser
import mysql.connector
import numpy as np
import re

class MysqlConnector():
    def __init__(self, db_file_path : str = 'database_conf.ini'):
        """
        Connect to MySQL server and database based on preset values. Return database object.

        :Input:
            This function require the OpenHI configuration file. Example conf can be found at "static/database_conf.ini".

        :return: MySQL database object
        """

        # Read the database configuration file.
        conf = configparser.ConfigParser()
        conf.read(db_file_path)

        # Read necessary information from dictionary object
        conf_host = conf['db']['host']
        conf_port = conf['db']['port']
        conf_user = conf['db']['user']
        conf_passwd = conf['db']['passwd']
        conf_database = conf['db']['database']

        # Create database object based on the given configuration
        self.mydatabase = mysql.connector.connect(
            host=conf_host,
            port=conf_port,
            user=conf_user,
            passwd=conf_passwd,
            database=conf_database
        )

    def get_tba_list(self, slide_id):
        """
        This function will fetch the to be annotated list from the MySQL database based on the give slide id.

        :param db: OpenHI database object.
        :param slide_id: (int) slide id
        :return: (list) tuple of x and y coordinate.
        """

        sql_cmd = 'SELECT sw_id, center_x, center_y FROM tba_list WHERE slide_id = ' + str(slide_id) + ';'
        # print(sql_cmd)

        mycursor = self.mydatabase.cursor()
        mycursor.execute(sql_cmd)
        myresult = mycursor.fetchall()
        mycursor.close()

        return myresult

    def get_center_tb_reg(self, slide_id, reg_id):
        sql_cmd = 'SELECT center_x, center_y FROM tba_list WHERE slide_id = ' \
                  + str(slide_id) + ' AND sw_id = ' + str(reg_id) + ';'

        mycursor = self.mydatabase.cursor()
        mycursor.execute(sql_cmd)
        myresult = mycursor.fetchall()
        mycursor.close()

        return myresult[0][0], myresult[0][1]