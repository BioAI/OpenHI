.. _ohi-coor:

OpenHI coordinate system
=========================

OpenHI uses specific terms to address images during the processing.

.. figure:: ../_static/coordinate_system.png

    OpenHI coordinate system

Refer to a region (area) in WSI
-------------------------------
Regions in WSI are referred to as **viewing area** in OpenHI. This is the green box in the image above. There are two parameters to describe this box.

1. Top-left coordinate
######################
The top-left coordinate is an anchor specifying where are you referring to in WSI (which is usually very big). In OpenHI, top-left coordinates came in pair :code:`(x, y)`. The minimum value is :code:`(0, 0)` and the maximum value depends on the size of particular WSI (:ref:`get WSI size <get-wsi-size>`) which equals to the bottom right or the :code:`width` (dashed blue line) and :code:`height` (dashed purple line) of the WSI.

.. _region-size:

2. Region size
##############
Depends on how large the area you want to view. **Region size** or (sometimes called :code:`viewing_size`) will specify the area you wish to view, thus this is called *viewing area*. The parameter also came in pair :code:`(width, height)` which came in :code:`int`.

Returning image
---------------
Users can specify a very large area in the WSI. Thus we need to specify the **viewer size** (blue rectangle) as well since we cannot just return very large image array (that would take up a large chunk of machine's memory and could crash the program in case someone request to view 100,000 by 100,000 area!). In the process of reading WSI files, down sampling is part of the job. In short, users must define how large they want the returning image is going to be so that the library can fetch the image at the correct level (it's a pyramidal image).

The :code:`viewer_size` consist of :code:`(width, height)` pair. It would be better if the size ratio match the :code:`viewing_size` (see :ref:`region size <region-size>`) so the output image would be squished. For example, when the down sampling factor (DSF) is :code:`2`:

.. code-block:: python

    viewing_size = (1000, 2000)
    viewer_size = (500, 1000)

Example usage:

.. py:method:: openhi.slide.Slide.read_region