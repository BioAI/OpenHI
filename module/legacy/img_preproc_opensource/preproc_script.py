"""
This is the main image pre-processing script of the framework. It will request full path to WSI from the connector.
"""
from skimage.segmentation import slic, mark_boundaries
# from skimage.util import img_as_float
from skimage import io
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import openslide
import sys
import time

# Internal packages
import dbv1_connector_mockup as dbcon
import tcga_connector as srccon


def imshow(input_cv_img):
    """
    Easy MATLAB-like imshow function just for development and debugging based on OpenCV GUI
    :param input_cv_img: input image compatible with image in OpenCV format (BGR)
    :return: (Window that displays the input image)
    """
    cv.imshow('Figure - remember to hit any key to close the window', input_cv_img)
    cv.waitKey(0)
    cv.destroyAllWindows()


def imshow_sk(input_skimage):
    """
    Easy MATLAB-like imshow function just for development and debugging based on matplotlib-pyplot
    :param input_skimage: input image compatible with skimage format
    :return: (Window that displays the input image)
    """
    fig = plt.figure("Figure")
    ax = fig.add_subplot(1, 1, 1)
    ax.imshow(input_skimage)
    plt.axis("off")
    plt.show()


def _progress(count, total, suffix=''):
    """
    Simple text-based progress bar
    by Andrea Griffini (https://stackoverflow.com/a/6169274)
    :param count: the number of iteration
    :param total: max iteration
    :param suffix: suffix
    :return: progress bar as text-output
    """
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  # As suggested by Rom Ruben


def _read_wsi(directory_read):
    """
    This function utilize OpenSlide library to read the WSI and return very large WSI as image array.
    :param directory_read: string of text that specify full path to the original WSI file.
    :return:
    """
    # Create OpenSlide pointer
    ptr = openslide.OpenSlide(directory_read)
    dim = ptr.dimensions

    # WSI reading loop
    vline_width = 1000
    nloop = int(dim[0] / vline_width)
    y1 = 0
    y2 = dim[1]

    # Create empty PIL image
    large_image = Image.new('RGBA', (dim[0], y2), color=(0, 0, 0, 255))

    for i in range(nloop):
        # print(i)
        x1 = vline_width * i
        # x2 = (vline_width * i) + vline_width-1

        tl_coor = (x1, y1)
        loop_size = (vline_width, y2)

        img = ptr.read_region(tl_coor, 0, loop_size)
        # print('Pasting at (TL coor): ' + str(x1) + 'x' + str(y1))
        large_image.paste(img, (x1, y1))

        _progress(i, nloop)

    # Stitching the final band
    x1 = vline_width * nloop
    # x2 = dim[0]
    tl_coor = (x1, y1)
    loop_size = (vline_width, y2)
    img = ptr.read_region(tl_coor, 0, loop_size)
    large_image.paste(img, (x1, y1))
    _progress(1, 1)
    # The last band pasted into the main image will be larger than available space. However it will be OK
    # because the excess will not be saved.

    # Delete OpenSlide pointer as best practice
    del ptr

    return large_image


def main_process(sid, superpixel_density):
    directory_read = srccon.get_fullpath(sid)  # slide_id starting from 0
    # Read WSI (mocked-up with normal image -- to be replaced with OpenSlide)
    image = io.imread('20x.png')
    # image = _read_wsi(directory_read)
    # TODO: Try to change PIL image format to scikit-image format. So that superpixel may work.

    # Measuring image properties
    dim = image.size  # returns in (height, width, depth) OR (row, column, depth)
    dim = (image.shape[1], image.shape[0])

    # Calculate number of sub-region needed
    pixel_total = dim[0] * dim[1]
    nseg = int(pixel_total / superpixel_density)

    # apply SLIC and extract (approximately) the supplied number of segments
    print('Superpixeling ...')
    print('Start: ' + str(time.ctime()))
    time_sp_start = time.time()
    segments = slic(image, n_segments=nseg, compactness=10.0, convert2lab=None, slic_zero=True, max_iter=10)
    print('Finished: ' + str(time.ctime()))
    time_sp_elapsed = time.time() - time_sp_start
    print('Elapsed time: ' + str(time_sp_elapsed/60) + ' m')

    # imshow_sk(mark_boundaries(image, segments))   # Not practical to show.

    # Convert labelling matrix to binary boundary matrix
    print('Boundary marking ...')
    print('Start: ' + str(time.ctime()))
    time_bm_start = time.time()
    img_blank = np.zeros((dim[1], dim[0], 1), dtype=np.uint8)
    # Note: for Numpy available data type, see: https://docs.scipy.org/doc/numpy-1.13.0/user/basics.types.html
    img_boundaries = mark_boundaries(img_blank, segments, color=255)
    print('Finished: ' + str(time.ctime()))
    time_bm_elapsed = time.time() - time_bm_start
    print('Elapsed time: ' + str(time_bm_elapsed/60) + ' m')

    directory_save = dbcon.get_boundary_filename(sid, superpixel_density)
    # Note: PIL.Image supports saving binary image while OpenCV, Matplotlib, and Scikit Image do not.
    # io.imsave(directory_save, img_boundaries)
    # cv.imwrite(directory_save, img_boundaries)
    # plt.imsave(directory_save, img_boundaries)
    img_boundaries_ = np.uint8(img_boundaries)
    # arr = np.array(img_boundaries_)
    # pil_img = Image.frombuffer(mode='L', dim=(dim[1], dim[0]), data=img_boundaries_)
    pil_img = Image.frombuffer("L", dim, img_boundaries_)
    pil_bin = pil_img.convert("1")
    pil_bin.save(directory_save)


if __name__ == '__main__':
    # Configuration
    conf = {
        "start_slide": "1",
        "stop_slide": "1600",
        "skip_slide": [1, 2, 3],
        "seg_density": [5000, 1000, 60],
    }
    print('entering main process')
    main_process(0, 5000)
    print('done')
    # conf = json.loads(in_script_conf)

