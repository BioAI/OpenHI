OpenHI Modules
===================

Suggestive annotation module
----------------------------

Neural nets models
##################

.. automodule:: module.suggest.models
   :members:

Prediction functions
####################

.. automodule:: module.suggest.predict
   :members:

Training functions
##################

.. automodule:: module.suggest.train
   :members:

WSI Downloading from NIH-GDC repository
---------------------------------------

.. automodule:: module.data_download.download
   :members:
