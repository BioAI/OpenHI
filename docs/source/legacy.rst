Legacy APIs
==================================

This page contains the legacy APIs working with OpenHIv1 system.

Image Compatibility
-------------------

Image compatibility functions preserves interoperability between different versions of OpenHI.

.. automodule:: openhi.img.legacy
   :members:


SQL Module
----------

.. automodule:: openhi.legacy.anno_sql
   :members:


Image processing module
-----------------------

.. automodule:: openhi.legacy.anno_img
   :members:


Web module
----------

.. automodule:: openhi.legacy.anno_web
   :members:

Other documentations
--------------------

.. toctree::
    :maxdepth: 1

    legacy/api-docs
    legacy/OpenHI_config
    legacy/mysql-database
    legacy/modules
    legacy/front-back-end_interface
