"""Test script of the main OpenHI platform

The tests include
- import the library

"""

import openhi.img.legacy as legacy
import openhi.img.imgproc as improc
import numpy as np
import unittest


class TestStringMethods(unittest.TestCase):
    """This is the example of test file."""

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    # def test_split(self):
    #     s = 'hello world'
    #     self.assertEqual(s.split(), ['hello', 'world'])
    #     # check that s.split fails when the separator is not a string
    #     with self.assertRaises(TypeError):
    #         s.split(2)

    # def test_get_original(self):
    #     result = legacy.get_original(1, 1, 0)
    #     self.assertEqual(type(result), np.ndarray)

    # def test_get_original_grid(self):
    #     result = legacy.get_original_grid(1, 1, 0)
    #     self.assertEqual(type(result), np.ndarray)

    # def test_get_annotation(self):
    #     result = legacy.get_original(1, 1, 0)
    #     self.assertEqual(type(result), np.ndarray)

    # def test_get_annotation_color(self):
    #     result = legacy.get_annotation_color(1, 1, 0)
    #     self.assertEqual(type(result), np.ndarray)

    # def test_get_annotation_grid(self):
    #     result = legacy.get_original_grid(1, 1, 0)
    #     self.assertEqual(type(result), np.ndarray)


if __name__ == '__main__':
    unittest.main()



