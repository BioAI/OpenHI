# WSI data of OpenHI 

In this `data` folder, WSIs can be stored with names according to the manifest. 

For example, this is the `manifest.txt` (located just outside this folder).

```
id	filename	md5	size	state
eb25474f-e249-4f39-9339-b8f5c1690610	TCGA-A3-A6NN-01Z-00-DX1_4769C9A7-02D2-4080-8AA1-CD34F11CB344.svs	21a64c9e2d507531ace2cc6e618b5322	328941856	validated
be1b829c-c8e1-468f-969d-36050b4c2f78	TCGA-A3-3335-01Z-00-DX1_b36f9bd5-d005-4d81-ac3e-c768502ca06b.svs	bbdd0dbc410722312aa19796245a0ef2	354104719	validated
...
1b9b7ce6-7dad-4fac-a573-dcbf7127d473	TCGA-A3-A8OW-01Z-00-DX1_B173FC44-8708-4255-AF01-9D8194F0BCA1.svs	646eb9276e83d9980f2a7deec0d50d7e	435416026	validated
dab62c47-a50f-402f-8146-aa6bcf946db9	TCGA-A3-A8OU-01Z-00-DX1_5BF363CE-9DB1-40BB-9E77-CF930F12B1B8.svs	b4b6be5d9eba242c3f512dcf71b5a72d	440182180	validated
12634a67-6d76-4934-9270-707d68a494b2	TCGA-CZ-5982-01Z-00-DX1_b7659f4b-921e-42d9-a691-2b2262a2bfb3.svs	080027651dc8446b96e5ffaf871cf26b	451789473	validated
```

The WSI file represented by the first line (technically the second line since the first line is heading) should have a folder using id as folder name and WSI file right inside as follow. 

```
/framework_src/data/{id}/{filename}
``` 

For example: 

```
/framework_src/data/eb25474f-e249-4f39-9339-b8f5c1690610/TCGA-A3-A6NN-01Z-00-DX1_4769C9A7-02D2-4080-8AA1-CD34F11CB344.svs
```

To download slides from TCGA (GDC), see the [data downloading module](../../module/data_download/README.md). 